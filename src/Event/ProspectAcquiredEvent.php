<?php

namespace Pay4Later\Event;

class ProspectAcquiredEvent extends AbstractMessage implements EventInterface
{
    const VERSION = '1';
    const NAME = 'prospect_acquired';

    /** @var string */
    private $campaignId;

    /** @var string */
    private $firstName;

    /** @var string */
    private $lastName;

    /** @var string */
    private $company;

    /** @var string */
    private $phone;

    /** @var string */
    private $email;

    /** @var string */
    private $websiteAddress;

    /** @var string */
    private $turnover;

    /** @var string */
    private $sector;

    /** @var string */
    private $comments;

    /** @var string */
    private $optedOut;

    /** @var string */
    private $source;

    /** @var string */
    private $pardotVisitorId;

    public function __construct(
        $campaignId,
        $firstName,
        $lastName,
        $company,
        $phone,
        $email,
        $websiteAddress,
        $turnover,
        $sector,
        $comments,
        $optedOut,
        $source,
        $pardotVisitorId = null,
        $occurredAt = null
    ) {
        parent::__construct(self::NAME, self::VERSION, $occurredAt);
        $this->campaignId      = $campaignId;
        $this->firstName       = $firstName;
        $this->lastName        = $lastName;
        $this->company         = $company;
        $this->phone           = $phone;
        $this->email           = $email;
        $this->websiteAddress  = $websiteAddress;
        $this->turnover        = $turnover;
        $this->sector          = $sector;
        $this->comments        = $comments;
        $this->optedOut        = $optedOut;
        $this->source          = $source;
        $this->pardotVisitorId = $pardotVisitorId;
    }

    /**
     * @return array
     */
    public function getPayload()
    {
        return [
            'campaignId'      => $this->campaignId,
            'firstName'       => $this->firstName,
            'lastName'        => $this->lastName,
            'company'         => $this->company,
            'phone'           => $this->phone,
            'email'           => $this->email,
            'websiteAddress'  => $this->websiteAddress,
            'turnover'        => $this->turnover,
            'sector'          => $this->sector,
            'comments'        => $this->comments,
            'optedOut'        => $this->optedOut,
            'source'          => $this->source,
            'pardotVisitorId' => $this->pardotVisitorId
        ];
    }
}
