<?php

namespace Pay4Later\Event;

interface MessageInterface
{
    /**
     * @return string
     */
    public function getName();

    /**
     * @return string
     */
    public function getVersion();

    /**
     * @return array
     */
    public function getPayload();

    /**
     * @return \DateTimeInterface
     */
    public function getOccurredAt();
}
