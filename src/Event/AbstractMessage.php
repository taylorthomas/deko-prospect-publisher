<?php

namespace Pay4Later\Event;

abstract class AbstractMessage
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $version;

    /**
     * @var \DateTimeInterface
     */
    private $occurredAt;

    /**
     * @param $name
     * @param $version
     * @param null $occurredAt
     */
    public function __construct($name, $version, $occurredAt = null)
    {
        $this->name = $name;
        $this->version = $version;
        $this->occurredAt = is_null($occurredAt) ? new \DateTimeImmutable('@'. time()) : $occurredAt;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getOccurredAt()
    {
        return $this->occurredAt;
    }
}
