<?php

namespace Pay4Later\Event\Encode;

use Pay4Later\Event\MessageInterface;

class JsonMessageEncoder implements MessageEncoderInterface
{
    const NAME = 'name';
    const VERSION = 'version';
    const PAYLOAD = 'payload';
    const OCCURRED_AT = 'occurred_at';

    /**
     * @param MessageInterface $event
     * @return string
     */
    public function encode(MessageInterface $event)
    {
        return json_encode([
            self::NAME => $event->getName(),
            self::VERSION => $event->getVersion(),
            self::OCCURRED_AT => $event->getOccurredAt()->format('c'),
            self::PAYLOAD => $event->getPayload(),
        ]);
    }
}
