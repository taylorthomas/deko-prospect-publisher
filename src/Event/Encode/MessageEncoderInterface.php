<?php

namespace Pay4Later\Event\Encode;

use Pay4Later\Event\MessageInterface;

interface MessageEncoderInterface
{
    /**
     * @param MessageInterface $message
     * @return string
     */
    public function encode(MessageInterface $message);
}
