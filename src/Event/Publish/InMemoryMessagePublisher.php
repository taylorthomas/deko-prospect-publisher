<?php

namespace Pay4Later\Event\Publish;

use Pay4Later\Event\MessageInterface;

class InMemoryMessagePublisher implements MessagePublisherInterface
{
    private $events = [];

    /**
     * @param MessageInterface $event
     */
    public function publish(MessageInterface $event)
    {
        $this->events[] = $event;
    }

    /**
     * @return MessageInterface[]
     */
    public function getPublishedEvents()
    {
        return $this->events;
    }
}
