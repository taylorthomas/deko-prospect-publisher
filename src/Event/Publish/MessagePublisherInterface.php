<?php

namespace Pay4Later\Event\Publish;

use Pay4Later\Event\MessageInterface;

interface MessagePublisherInterface
{

    /**
     * @param MessageInterface $event
     */
    public function publish(MessageInterface $event);
}
