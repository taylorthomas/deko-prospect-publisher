<?php

namespace Pay4Later\Event\Publish;

use Aws\Sns\SnsClient;
use Pay4Later\Event\Encode\MessageEncoderInterface;
use Pay4Later\Event\MessageInterface;

class SnsMessagePublisher implements MessagePublisherInterface
{
    /** @var SnsClient $client */
    private $client;

    /** @var  MessageEncoderInterface $encoder */
    private $encoder;

    /** @var string $arn */
    private $arn;

    /**
     * @param SnsClient $client
     * @param MessageEncoderInterface $encoder
     * @param string $arn
     */
    public function __construct(SnsClient $client, MessageEncoderInterface $encoder, $arn)
    {
        $this->client = $client;
        $this->encoder = $encoder;
        $this->arn = $arn;
    }

    /**
     * @param MessageInterface $event
     */
    public function publish(MessageInterface $event)
    {
        $this->client->publish(
            [
                'Message' => $this->encoder->encode($event),
                'TopicArn' => $this->arn
            ]
        );
    }
}
