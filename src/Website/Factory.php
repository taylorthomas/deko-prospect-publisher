<?php
namespace Pay4Later\Website;

class Factory
{
    public static function getProspectPublisher(
        array $post,
        array $cookie,
        string $environment,
        string $aws_key,
        string $aws_secret
    ) : ProspectPublisher {
        return new ProspectPublisher(
            $post,
            $cookie,
            $environment,
            $aws_key,
            $aws_secret
        );
    }
}
