<?php
namespace Pay4Later\Website;

use Aws\Sns\SnsClient;
use Pay4Later\Event\Encode\JsonMessageEncoder;
use Pay4Later\Event\Publish\SnsMessagePublisher;
use Pay4Later\Event\ProspectAcquiredEvent;

class ProspectPublisher
{
    /** @var array $post */
    private $post;

    /** @var array $cookie */
    private $cookie;

    /** @var string $environment */
    private $environment;
    
    /** @var string $aws_key */
    private $aws_key;

    /** @var string $aws_secret */
    private $aws_secret;


    /**
     * @param array $post
     * @param array $cookie
     */
    public function __construct(
        array $post,
        array $cookie,
        string $environment,
        string $aws_key,
        string $aws_secret
    ) {
        $this->post        = $post;
        $this->cookie      = $cookie;
        $this->environment = $environment;
        $this->aws_key     = $aws_key;
        $this->aws_secret  = $aws_secret;
    }

    /**
     * @return ProspectAcquiredEvent
     */
    private function buildProspectAcquiredEvent()
    {
        $campaignId     = null;
        $firstName      = $this->post['first-name']      ? strip_tags($this->post['first-name'])      : null;
        $lastName       = $this->post['last-name']       ? strip_tags($this->post['last-name'])       : null;
        $companyName    = $this->post['company-name']    ? strip_tags($this->post['company-name'])    : null;
        $phoneNumber    = $this->post['phone-number']    ? strip_tags($this->post['phone-number'])    : null;
        $emailAddress   = $this->post['email-address']   ? strip_tags($this->post['email-address'])   : null;
        $websiteAddress = $this->post['website-address'] ? strip_tags($this->post['website-address']) : null;
        $turnover       = $this->post['turnover']        ? strip_tags($this->post['turnover'])        : null;
        $sector         = $this->post['sector']          ? strip_tags($this->post['sector'])          : null;
        $comments       = $this->post['comments']        ? strip_tags($this->post['comments'])        : null;
        $optedOut       = !$this->post['opt-in'];

        $pardotAccountId         = 73452;
        $pardotVisitorCookieName = "visitor_id$pardotAccountId";
        $pardotVisitorId         = isset($this->cookie[$pardotVisitorCookieName]) ? $this->cookie[$pardotVisitorCookieName] : null;

        return new ProspectAcquiredEvent(
            $campaignId,
            $firstName,
            $lastName,
            $companyName,
            $phoneNumber,
            $emailAddress,
            $websiteAddress,
            $turnover,
            $sector,
            $comments,
            $optedOut,
            'Deko Website - ' . $this->environment,
            $pardotVisitorId
        );
    }

    public function publish()
    {
        $message = $this->buildProspectAcquiredEvent();

        $sns = new SnsClient([
            'version'     => '2010-03-31',
            'region'      => 'eu-west-1',
            'credentials' => [
                'key'    => $this->aws_key,
                'secret' => $this->aws_secret
            ]
        ]);

        $messagePublisher = new SnsMessagePublisher(
            $sns,
            new JsonMessageEncoder(),
            'arn:aws:sns:eu-west-1:428466588005:production-corporate-website'
        );

        $messagePublisher->publish($message);
    }
}
