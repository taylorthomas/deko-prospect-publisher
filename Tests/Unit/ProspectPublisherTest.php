<?php

namespace Pay4Later\Website\Tests\Unit;

use Pay4Later\Website\Factory;
use PHPUnit\Framework\TestCase;
use Dotenv\Dotenv;
use VCR\VCR;
use allejo\VCR\VCRCleaner;
use DateTime;
use Exception;

require 'internalFunctionOverrides.php';


class ProspectPublisherTest extends TestCase
{

    const MERCHANT_DATA = [
        'first-name'         => 'Stephen',
        'last-name'          => 'Merchant',
        'email-address'      => 'steve@example.com',
        'phone-number'       => '0800 123 456',
        'website-address'    => '0800 123 456',
        'turnover'           => '1 million',
        'opt-in'             => '1'
    ];

    const LENDER_DATA = [
        'first-name'         => 'Lee',
        'last-name'          => 'Lender',
        'email-address'      => 'lee@example.com',
        'phone-number'       => '0370 000 000',
        'company-name'       => 'Wonga',
        'comments'           => 'Test comments.',
        'opt-in'             => '1'
    ];

    const ENVIRONMENT = 'test';
    const COOKIE      = ['visitor_id73452' => '1234567890'];

    private $awsKey;
    private $awsSecret;

    public static $uniqid;
    public static $gmdate;
    public static $time;


    public function setUp()
    {
        $dotenv = new Dotenv(__DIR__);
        $dotenv->load();
        $this->awsKey    = getenv('DEKO_AWS_KEY');
        $this->awsSecret = getenv('DEKO_AWS_SECRET');

        VCRCleaner::enable([
            'ignoreHeaders' => [
                'Authorization'
            ]
        ]);
    }


    /**
     * @expectedException Aws\Sns\Exception\SnsException
     */
    public function testIncorrectCredentials()
    {
        $publisher = Factory::getProspectPublisher(
            self::MERCHANT_DATA,
            self::COOKIE,
            self::ENVIRONMENT,
            'bad-key',
            'bad-secret'
        );

        VCR::turnOn();
        VCR::insertCassette('incorrect_credentials');

        $publisher->publish();

        VCR::eject();
        VCR::turnOff();
    }



    public function testEmptyPostDataIsAccepted()
    {
        $dateTime  = new DateTime('2018-11-29 14:37');
        $timestamp = $dateTime->getTimestamp();

        self::$uniqid = 'u4h5g0872h';
        self::$gmdate = gmdate('Ymd\THis\Z', $timestamp);
        self::$time   = $timestamp;

        VCR::turnOn();
        VCR::insertCassette('missing_data_test');

        $post = [];

        $publisher = Factory::getProspectPublisher(
            $post,
            self::COOKIE,
            self::ENVIRONMENT,
            $this->awsKey,
            $this->awsSecret
        );

        $result = $publisher->publish();

        $this->assertNull($result);

        VCR::eject();
        VCR::turnOff();

        self::$uniqid = null;
        self::$gmdate = null;
        self::$time   = null;
    }



    public function testCompletedMerchantDataIsAccepted()
    {
        $dateTime  = new DateTime('2018-11-29 14:45');
        $timestamp = $dateTime->getTimestamp();

        self::$uniqid = 'jtglwejkbgl';
        self::$gmdate = gmdate('Ymd\THis\Z', $timestamp);
        self::$time   = $timestamp;

        VCR::turnOn();
        VCR::insertCassette('completed_merchant_data_test');

        $publisher = Factory::getProspectPublisher(
            self::MERCHANT_DATA,
            self::COOKIE,
            self::ENVIRONMENT,
            $this->awsKey,
            $this->awsSecret
        );

        $result = $publisher->publish();

        $this->assertNull($result);

        VCR::eject();
        VCR::turnOff();

        self::$uniqid = null;
        self::$gmdate = null;
        self::$time   = null;
    }



    public function testCompletedLenderDataIsAccepted()
    {
        $dateTime  = new DateTime('2018-11-29 14:49');
        $timestamp = $dateTime->getTimestamp();

        self::$uniqid = '459h34508g7';
        self::$gmdate = gmdate('Ymd\THis\Z', $timestamp);
        self::$time   = $timestamp;

        VCR::turnOn();
        VCR::insertCassette('completed_lender_data_test');

        $publisher = Factory::getProspectPublisher(
            self::LENDER_DATA,
            self::COOKIE,
            self::ENVIRONMENT,
            $this->awsKey,
            $this->awsSecret
        );

        $result = $publisher->publish();

        $this->assertNull($result);

        VCR::eject();
        VCR::turnOff();

        self::$uniqid = null;
        self::$gmdate = null;
        self::$time   = null;
    }
}
