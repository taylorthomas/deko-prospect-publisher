<?php

namespace Aws {
    function uniqid(string $prefix = "", bool $more_entropy = false)
    {
        return \Pay4Later\Website\Tests\Unit\ProspectPublisherTest::$uniqid ?: \uniqid($prefix, $more_entropy);
    }
}

namespace Aws\Signature {
    function gmdate(string $format, int $timestamp = 0)
    {
        if (!$timestamp) {
            $timestamp = time();
        }
        return \Pay4Later\Website\Tests\Unit\ProspectPublisherTest::$gmdate ?: \gmdate($format, $timestamp);
    }
}

namespace Pay4Later\Event {
    function time()
    {
        return \Pay4Later\Website\Tests\Unit\ProspectPublisherTest::$time ?: \time();
    }
}
